
Public Class RemoteDeleteClientObject
    Inherits MarshalByRefObject

    Public Function Delete(ByVal file As String) As Object
        RaiseEvent DeleteFile(Me, New DeleteArgs(file))
    End Function

    Public Shared Event DeleteFile(ByVal sender As Object, ByVal a As DeleteArgs)

End Class