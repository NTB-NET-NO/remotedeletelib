Imports System.IO
' Imports System.Runtime.Remoting
Imports System.Messaging
Imports ntb_FuncLib.LogFile

Public Class RemoteDeleteEventHandler
    Inherits MarshalByRefObject

    'Variables
    Private _serverMode As Boolean
    Private _useMsq As Boolean
    Private _lastfile As String

    Private _logFolder As String

    Private _offlineFiles As ArrayList
    Private _offlineDates As ArrayList

    'Sync object
    ReadOnly _isBusy As Threading.AutoResetEvent = New Threading.AutoResetEvent(False)

    'Remote objects
    Dim _client As RemoteDeleteClientObject
    Dim _server As RemoteDeleteServerObject

    'Message Queuing
    Dim WithEvents LocalQueue As MessageQueue
    Dim WithEvents RemoteQueue As MessageQueue

    'Event generators
    Private WithEvents offlineTimer As New Timers.Timer
    Private WithEvents fileDeleteWatcher As New FileSystemWatcher

    'Kickstart all jobs and timers
    Public Sub Init(ByVal svr As Boolean)

        _serverMode = svr
        _offlineFiles = New ArrayList
        _offlineDates = New ArrayList

        _logFolder = Configuration.ConfigurationManager.AppSettings("logFolder")
        Directory.CreateDirectory(_logFolder)

        WriteLog(_logFolder, "DeleteSyncService starting...")

        ' Remoting
        If _serverMode Then
            AddHandler RemoteDeleteServerObject.DeleteFile, AddressOf On_RemoteDeleteFile
        Else
            AddHandler RemoteDeleteClientObject.DeleteFile, AddressOf On_RemoteDeleteFile
        End If

        ' Message queueing
        _useMsq = Configuration.ConfigurationManager.AppSettings("enableMSQ")
        If _useMsq Then
            LocalQueue = New MessageQueue(Configuration.ConfigurationManager.AppSettings("localQueue"))
            RemoteQueue = New MessageQueue(Configuration.ConfigurationManager.AppSettings("remoteQueue"))

            If Not MessageQueue.Exists(LocalQueue.Path) Then
                MessageQueue.Create(LocalQueue.Path)
                WriteLog(_logFolder, "Message queue created (local): " & LocalQueue.Path)
            End If

            If Not MessageQueue.Exists(RemoteQueue.Path) Then
                MessageQueue.Create(RemoteQueue.Path)
                WriteLog(_logFolder, "Message queue created (remote): " & RemoteQueue.Path)
            End If

            LocalQueue.BeginReceive()
        End If

        'Filesystem watcher
        Directory.CreateDirectory(Configuration.ConfigurationManager.AppSettings("deleteSyncPath"))

        fileDeleteWatcher.Path = Configuration.ConfigurationManager.AppSettings("deleteSyncPath")
        fileDeleteWatcher.InternalBufferSize = Configuration.ConfigurationManager.AppSettings("fileWatcherBuffer")
        fileDeleteWatcher.IncludeSubdirectories = Configuration.ConfigurationManager.AppSettings("subDirs")
        fileDeleteWatcher.NotifyFilter = NotifyFilters.FileName Or NotifyFilters.LastWrite Or NotifyFilters.DirectoryName
        fileDeleteWatcher.EnableRaisingEvents = True

        'Offline timer
        offlineTimer.Interval = Configuration.ConfigurationManager.AppSettings("offlineInterval") * 1000
        offlineTimer.Start()

        WriteLog(_logFolder, "DeleteSyncService started successfully.")

        _isBusy.Set()
    End Sub

    'Remote machine file deletion
    Public Sub On_RemoteDeleteFile(ByVal sender As Object, ByVal args As DeleteArgs)
        _isBusy.WaitOne()

        Try
            DeleteFile(args.filename, "REMOTING")
        Catch ex As Exception
            WriteErr(_logFolder, "REMOTING RECV - Sletting FEILET: " & args.filename, ex)
        End Try

        _isBusy.Set()
    End Sub

    'Offline check
    Private Sub offlineTimer_Elapsed(ByVal sender As Object, ByVal e As Timers.ElapsedEventArgs) Handles offlineTimer.Elapsed

        _isBusy.WaitOne()

        Dim status As Boolean = False
        Dim i
        i = 0
        For i = _offlineFiles.Count - 1 To 0 Step -1

            If File.Exists(_offlineFiles(i)) Then
                _lastfile = _offlineFiles(i)
                Try
                    File.Delete(_offlineFiles(i))
                    Threading.Thread.Sleep(100)
                    WriteLog(_logFolder, "OFFLINE - Slettet fil lokalt: " & _offlineFiles(i))
                Catch ex As Exception
                    WriteErr(_logFolder, "OFFLINE - Sletting FEILET: " & _offlineFiles(i), ex)
                End Try

                _offlineFiles.RemoveAt(i)
                _offlineDates.RemoveAt(i)

                status = True
            ElseIf Now.Subtract(_offlineDates(i)).totalminutes > 60 Then
                _offlineFiles.RemoveAt(i)
                _offlineDates.RemoveAt(i)

                status = True
            End If

        Next

        If status Then
            Try
                WriteLog(_logFolder, "OFFLINE status: " & _offlineFiles.Count & " filer.")
            Catch ex As Exception
                WriteErr(_logFolder, "OFFLINE - status FEIL: ", ex)
            End Try
        End If

        _isBusy.Set()
    End Sub

    'Filesystem watch
    Private Sub fileDeleteWatcher_Deleted(ByVal sender As Object, ByVal e As FileSystemEventArgs) Handles fileDeleteWatcher.Deleted
        Dim msq As Boolean = False

        ' Definition below is marked out because it is not in use
        ' Dim prop As IDictionary 

        '_isBusy.WaitOne()

        Dim fileName As String = e.FullPath

        'Check if we just deleted this file after remote command
        If _lastfile = fileName Then
            _lastfile = ""
        Else

            'Try remote deletion
            Try
                If _client Is Nothing And _server Is Nothing Then
                    If _serverMode Then
                        _client = New RemoteDeleteClientObject
                    Else
                        _server = New RemoteDeleteServerObject
                    End If
                End If

                If _serverMode Then
                    _client.Delete(e.FullPath)
                Else
                    _server.Delete(e.FullPath)
                End If

                WriteLog(_logFolder, "REMOTING SEND - Slettet fil remote: " & fileName)
            Catch ex As Exception
                msq = _useMsq
                WriteErr(_logFolder, "REMOTING SEND - Sletting feilet: " & fileName, ex)
            End Try

            'Otherwise send command by message queueing
            If msq Then
                Try
                    RemoteQueue.Send(fileName, Path.GetFileName(fileName))
                    WriteLog(_logFolder, "MSQ SEND - Sletter fil remote: " & fileName)
                Catch ex As Exception
                    WriteErr(_logFolder, "MSQ SEND - Sletting feilet: " & fileName, ex)
                End Try
            End If

        End If

        '_isBusy.Set()
    End Sub

    Private Sub LocalQueue_ReceiveCompleted(ByVal sender As Object, ByVal e As ReceiveCompletedEventArgs) Handles LocalQueue.ReceiveCompleted
        _isBusy.WaitOne()

        Dim targetTypes(0) As Type
        targetTypes(0) = GetType(String)
        Dim formatter As XmlMessageFormatter = New XmlMessageFormatter(targetTypes)
        e.Message.Formatter = formatter

        Try
            DeleteFile(e.Message.Body(), "MSQ")
        Catch ex As Exception
            WriteErr(_logFolder, "MSQ RECV - Sletting FEILET: " & e.Message.Body(), ex)
        End Try

        _isBusy.Set()
        LocalQueue.BeginReceive()
    End Sub

    Sub DeleteFile(ByVal filename As String, ByVal mode As String)

        'Track files deleted here... prevent sending them to outgoing queue
        _lastfile = filename

        'Add messages to offline, they may not be created yet
        If Not File.Exists(filename) Then
            _offlineFiles.Add(filename)
            _offlineDates.Add(Now)

            WriteLog(_logFolder, mode & " RECV - Fil lagt til OFFLINE (" & _offlineFiles.Count & " i k�): " & filename)
        Else
            Try
                File.Delete(filename)
                Threading.Thread.Sleep(50)
                WriteLog(_logFolder, mode & " RECV - Slettet fil lokalt: " & filename)
            Catch ex As Exception
                _offlineFiles.Add(filename)
                _offlineDates.Add(Now)

                WriteLog(_logFolder, mode & " RECV - Sletting FEILET, lagt til OFFLINE: " & filename)
                WriteErr(_logFolder, mode & " RECV - Sletting FEILET: " & filename, ex)
            End Try

        End If

    End Sub
End Class
